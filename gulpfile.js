// Include gulp
var gulp = require('gulp');
 
// Include Plugins/Modules
var jshint = require('gulp-jshint'),
   sass = require('gulp-sass'),
   concat = require('gulp-concat'),
   uglify = require('gulp-uglify'),
   rename = require('gulp-rename'),
   cssnano = require('gulp-cssnano'),
   livereload = require('gulp-livereload'),
   copy = require('gulp-copy'),
   autoprefixer = require('gulp-autoprefixer');
 
// Lint Task
gulp.task('lint', function () {
   return gulp.src('src/js/*.js')
       .pipe(jshint())
       .pipe(jshint.reporter('default'));
});
 
 
// copy necessary files for production to seperate root folder
gulp.task('copy', function () {
  return gulp.src(['*/*.php', '*.php', '!wwwroot/*', 'src/fonts/*'])
      .pipe(copy('wwwroot', { prefix: 0 }));
});
 
// Compile Sass, prefix css, minify and rename
gulp.task('styles', function () {
   return gulp.src('scss/*.scss')
       .pipe(sass())
       .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
       .pipe(rename({ suffix: '.min' }))
       .pipe(cssnano())
       .pipe(gulp.dest('wwwroot/css'));
});
 
// Concatenate & Minify JS
gulp.task('scripts', function () {
   return gulp.src('js/*.js')
       .pipe(concat('scripts.js'))
       .pipe(gulp.dest('wwwroot'))
       .pipe(rename({ suffix: '.min' }))
       .pipe(uglify())
       .pipe(gulp.dest('wwwroot/js'));
});
 
// watch for changes in js and scss files
gulp.task('watch:changes', function () {
    livereload.listen();
    gulp.watch(['js/*.js', 'scss/*/*.scss', '*/*.php', '*.php', '!wwwroot/*'], gulp.series('styles', 'scripts', 'copy'));
  });
   
  gulp.task('watch', gulp.series(
    gulp.parallel('watch:changes')
  ));
 
 
gulp.task('default', gulp.series(['lint', 'styles', 'scripts', 'watch'] ));

gulp.task('message', function(){
  return console.log("Gulp running..")
});